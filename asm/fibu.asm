;-----------------------------------------------------------------;
; Datei        : fibu_ext.asm                                     ;
; Beschreibung : gibt Fibunaccireihe aus                          ;
;-----------------------------------------------------------------;

        org X"00000000" ; Programmstartadresse
        start init      ; markiert den ersten Befehl

; Immediate-Werte

        zero equ X"0000" ; Offset 0
        disp equ X"0FFC" ; Offset 0x0FFC, 0x0000_0FFC Adresse Displaylatch
        ram  equ X"0FF8" ; Offset 0x0FF8, 0x0000_0FF8 Basisadresse RAM

; r30 ram_pointer
init:
        lw.i r30, ram(r0)   ; r30 <- M[0x0000_0FF8]
        j b_ext


; r21 positiv a(n-2)
; r22 positiv a(n-1)
; r23 positiv a(n)
; r27 = 9 counter
b_ext   lw.i r30, ram(r0)   ; r30 <- M[0x0000_0FF8]
        and r21, r0, r0     ; r21 = 0
        add.i r22, r0, 1    ; r22 = 1
        add.i r27, r0, 9    ; r27 = 9
; a1 = r1 = 0
; a2 = r2 = 1
n_ext   add r23, r21, r22   ; r23 = r21 + r22
        add r21, r0, r22    ; r21 = r22
        add r22, r0, r23    ; r22 = r23
        sw.i zero(r30), r23 ; ram <- r23
        add.i r30, r30, 4   ; r30 zeigt auf nächste Speicherzelle
        beqz r27, f_ext
        sub.i r27, r27, 1   ; r27 = r27 - 1
        j n_ext
f_ext   j b_bas

; r21 positiv a(n-2)
; r22 negativ a(n-1)
; r23 positiv a(n)
; r24 = -4 (um ram um 4 zu erhöhen)
; r27 = 9 counter
; r28 = 1 (um counter zu verringern)
b_bas   slt r28, r0, r30    ; r28 = 1
        sub r21, r0, r0     ; r21 = 0
        sub r22, r0, r28    ; r22 = -1
        sub r24, r22, r28   ; r24 = -2
        sub r24, r24, r28   ; r24 = -3
        sub r24, r24, r28   ; r24 = -4
        sub r27, r0, r24    ; r27 = 4
        sub r27, r27, r24   ; r27 = 8
        sub r27, r27, r22   ; r27 = 9
; a1 = r1 = 0
; a2 = r2 = -1
n_bas   sub r23, r21, r22   ; r23 = r21 - r22
        sub r21, r0, r22    ; r21 = -r22
        sub r22, r0, r23    ; r22 = -r23
        sw.i zero(r30), r23 ; ram <- r23
        sub r30, r30, r24   ; r30 zeigt auf nächste Speicherzelle
        beqz r27, f_bas
        sub r27, r27, r28   ; r27 = r27 - 1
        j n_bas
f_bas   j ende

ende    j ende
end
