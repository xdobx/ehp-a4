
;-----------------------------------------------------------------;
; Datei        : fibu_basic.asm                                   ;
; Beschreibung : gibt Fibunaccireihe aus                          ;
;-----------------------------------------------------------------;

        org X"00000000" ; Programmstartadresse
        start init      ; markiert den ersten Befehl

; Immediate-Werte

        zero equ X"0000" ; Offset 0
        disp equ X"0FFC" ; Offset 0x0FFC, 0x0000_0FFC Adresse Displaylatch
        ram  equ X"0FF8" ; Offset 0x0FF8, 0x0000_0FF8 Basisadresse RAM

; r21 positiv a(n-2)
; r22 positiv a(n-1)
; r23 positiv a(n)
; r27 = 9 counter
; r30 ram_pointer

init:
        lw.i r30, ram(r0)   ; r30 <- M[0x0000_0FF8]
        and r21, r0, r0     ; r21 = 0
        add.i r22, r0, 1    ; r22 = 1
        add.i r27, r0, 9    ; r27 = 9
; a1 = r1 = 0
; a2 = r2 = 1
nfibu   add r23, r21, r22   ; r23 = r21 + r22
        add r21, r0, r22    ; r21 = r22
        add r22, r0, r23    ; r22 = r23
        sw.i zero(r30), r23 ; ram <- r23
        add.i r30, r30, 4   ; r30 zeigt auf nächste Speicherzelle
        beqz r27, ende
        sub.i r27, r27, 1   ; r27 = r27 - 1
        j nfibu
ende    j ende
end


