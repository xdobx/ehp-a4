;----------------------------------------------------------------;
;                                                                ;
; Datei         : dual_bcd.asm                                   ;
; Datum         : Mar. 2010                                      ;
; Beschreibung  : Dual-BCD-Wandlung                              ;
;----------------------------------------------------------------;

	org   X"00000000"  ; legt die Programmstartadresse fest 
        start init	   ; markiert den ersten Befehl
	 
; Immediate-Werte fuer Speicheroperationen:
        zero  equ  X"0000" ; Offset 0 
        disp  equ  X"0FFC" ; zur Adressierung des letzten 
                           ; Wortes im ROM (4KByte)
        ram   equ  X"0FF8" ; zur Adressierung des vorletzten 
                           ; Wortes im ROM
; Die letzten beiden ROM-Speicherzellen sind mit zwei Konstanten 
; initialisiert. In der Speicherzelle mit der Adresse 0x0000_0FFC 
; steht die Adresse fuer das Display Latch und in der Speicherzelle
; 0x0000_0FF8 wurde die Basisadresse des RAM abgelegt. 

; Dual-BCD-Wandlung:
; -----------------
; Die Dualzahl wird zunächst in das Register r1 geladen. und dann
; bitweise von rechts nach links in das BCD-
; Register r2 geschoben. Alle von der BCD-Zahl belegten Tetraden 
; werden von rechts beginnend getestet. Ist der Wert einer 
; Tetrade  > 4, dann wird vor dem Schieben der Korrekturwert 3 
; zu dieser Tetrade addiert. Das entspricht der Addition von 6 
; nach dem Schieben. Der bitweise Schiebevorgang geschieht, indem 
; die BCD-Zahl um eine Bitstelle nach links geschoben wird(bzw. 
; um i Stellen nach rechts gegenueber dem Schiebevorgang der 
; vorherigen Iteration). Das entsprechende Dualzahlbit wird der 
; BCD-Zahl dann rechts angefuegt.
; Die groesste hier als BCD-Zahl darstellbare Dualzahl lautet 
; X"5f5e0ff" (d"99999999"):
; dual_high equ X"5f5"
; dual_low  equ X"e0ff" 
;
; Die in eine BCD-Zahl zu wandelnde Dualzahl sei:
dual_high equ X"0"  
dual_low  equ X"ae" 
;
; Die Binaerdarstellung dieser Dualzahl erfordert 1 Tetrade und 1 Bit:
dualbits   equ 8
; Fuer die Darstellung der BCD-Zahl sind 2 Ziffern erforderlich:
digits     equ 2
;		  
;  verwendete Register:
;  -------------------
; r1  Dualzahl
; r2  aktueller BCD-Wert 
; r3  initiale Tetradenmaske (X"f")
; r4  aktuelle Tetradenmaske
; r5  initialer Tetradenvergleichswert (4+1)
; r6  aktueller Tetradenvergleichswert 
; r7  initialer Tetradenkorrekturwert (3)
; r8  aktueller Tetradenkorrekturwert
; r9  Laufvariable i (Dualzahlindex)
; r10 Tetradenindex
; r11 Sprungbedingung fuer Tetradenkorrektur 
; r12 Abbruchbedingung fuer die Tetradenkorrektur
; r13 aktueller Dualwert
; r14 aktuelles Dualzahlbit
; r31 Speicheradresse des Display-Latches

init:		
;; Initialisierung:	 
;; ---------------
	nop
ini_bcd	or.i r1,r0,dual_high
	sll.i r1,r1,16
	or.i r1,r1,dual_low
	add.i r9,r0, dualbits
	add.i r2,r0,0
	add.i r3,r0,X"f"
	add.i r5,r0,5
	add.i r7,r0,3
	lw.i r31,disp(r0) 	; r31 <- M[0x0000_0FFC]

;; BCD-Korrektur:
;; -------------	
bcd	sub.i r9,r9,1
	add.i r10,r0,X"fffc"	
	srl r13,r1,r9	
	and.i r14,r13,1
	sll.i r2,r2,1
	or r2,r14,r2	
	beqz r9,displ
te_ko	add.i r10,r10,4 
	sub.i r12,r10,(digits-1) * 4 
	sll r4,r3,r10
	sll r6,r5,r10
	sll r8,r7,r10
	and r14,r2,r4
	slt r11,r14,r6
	beqz r11,plus_3	
	beqz r12,bcd
	j te_ko			
plus_3	add r2,r2,r8
	beqz r12,bcd
	j te_ko
displ	sw.i zero(r31),r2
ende	j ende		
end
	
;;;------------------------------------------------------------

; C-Programm fuer die Dual-BCD-Wandlung der 4-Digit umfassenden
; Zahl 0x270f (0d9999):	
;
; void main()
;{
;    unsigned int bcd=0, dual;
;    dual  = 0x270f;
;    int i;
;    for(i=15;i>-1;i--)
;     {
;       if((bcd &  0xf)    > 0x4)
;	{bcd  += 0x3;}
;      if((bcd &  0xf0)   > 0x40)
;	{bcd  += 0x30;}
;      if((bcd &  0xf00)  > 0x400)
;	{bcd  += 0x300;}
;      if((bcd &  0xf000) > 0x4000)
;	{bcd  += 0x3000;}
;    printf("%2i  %4x  ",i, bcd);
;      bcd = bcd<<1;
;    printf("%4x  ",bcd);
;      bcd = ((dual>>i) & 0x1) | bcd;
;    }
;    printf("%4x\n",bcd);
;}
; Zwischenergebnisse:	
;;       <<1	  |bcd
;;15  :   0         0
;;14  :   0         0
;;13  :   0         1
;;12  :   2         2
;;11  :   4         4
;;10  :   8         9
;; 9  :  18        19
;; 8  :  38        39
;; 7  :  78        78
;; 6  : 156       156
;; 5  :	312       312
;; 4  : 624       624
;; 3  :1248      1249
;; 2  :2498      2499
;; 1  :4998      4999
;; 0  :9998      9999	
