;----------------------------------------------------------------;
;                                                                ;
; Datei         : dual_bcd.asm                                   ;
; Datum         : Mar. 2010                                      ;
; Beschreibung  : Dual-BCD-Wandlung                              ;
;----------------------------------------------------------------;
        org   X"00000000"  ; legt die Programmstartadresse fest 
        start init           ; markiert den ersten Befehl         
; Immediate-Werte fuer Speicheroperationen:
        zero  equ  X"0000" ; Offset 0 
        disp  equ  X"0FFC" ; zur Adressierung des letzten 
                           ; Wortes im ROM (4KByte)
        ram   equ  X"0FF8" ; zur Adressierung des vorletzten 
                           ; Wortes im ROM
;dual_high equ X"0"  ;- binär: 0000 0000 , dezimal:0 
;dual_low  equ X"ae" ;- binär: 1010 1110 , dezimal: 174
; Die Binaerdarstellung dieser Dualzahl erfordert 1 Tetrade und 1 Bit:
dualbits   equ 8
; Fuer die Darstellung der BCD-Zahl sind 2 Ziffern erforderlich:
digits     equ 2
;                  

; r30 ram_pointer
init:
        lw.i r30, ram(r0)   ; r30 <- M[0x0000_0FF8]
        j b_ext


; r21 positiv a(n-2)
; r22 positiv a(n-1)
; r23 positiv a(n)
; r27 = 9 counter
b_ext   and r21, r0, r0     ; r21 = 0
        add.i r22, r0, 1    ; r22 = 1
        add.i r27, r0, 9    ; r27 = 9
; a1 = r1 = 0
; a2 = r2 = 1
n_ext   add r23, r21, r22   ; r23 = r21 + r22
        add r21, r0, r22    ; r21 = r22
        add r22, r0, r23    ; r22 = r23
        sw.i zero(r30), r23 ; ram <- r23
        add.i r30, r30, 4   ; r30 zeigt auf nächste Speicherzelle
        beqz r27, f_ext
        sub.i r27, r27, 1   ; r27 = r27 - 1
        j n_ext
f_ext   j b_bcd

;  verwendete Register:
;  -------------------
; r1  Dualzahl
; r2  aktueller BCD-Wert 
; r3  initiale Tetradenmaske (X"f")
; r4  aktuelle Tetradenmaske
; r5  initialer Tetradenvergleichswert (4+1)
; r6  aktueller Tetradenvergleichswert 
; r7  initialer Tetradenkorrekturwert (3)
; r8  aktueller Tetradenkorrekturwert
; r9  Laufvariable i (Dualzahlindex)
; r10 Tetradenindex
; r11 Sprungbedingung fuer Tetradenkorrektur 
; r12 Abbruchbedingung fuer die Tetradenkorrektur
; r13 aktueller Dualwert
; r14 aktuelles Dualzahlbit
; r15 Schleifeninvariante für Umcodierung (6 = 8 - 2)
; r31 Speicheradresse des< Display-Latches

;- r30 aktuelles Zielregister
;- r18 aktueller Zeiger auf aktuelles Quellregster
;- r17 counter

;- aktuelle und initiale Register um verlust der "konstanten" vorzubeugen
;- hier wird die Grundlage der Umwandlung gelegt und die zu beginn gebrauchten Register initialisiert


b_bcd   lw.i r18, ram(r0)                        ;- Beginn des RAM, zeigt auf erste Fibu
        add.i r17, r0, 10                        ;- 10 Fibonaccizahlen zum ausgeben

ini_bcd lw.i r1, zero(r18)                       ;- dual, hole aktuelle Fibonaccizahl
        add.i r18, r18, 4                        ;- auf nächste Fibonaccizahl zeigen

        add.i r9,r0, dualbits                    ;- i, r9 = X"0000_0008", wir haben brauchen für jede Stelle einen Iterationsschritt

        add.i r2,r0,0                            ;- bcd, r2 = 0
        add.i r3,r0,X"f"                         ;- Tetrademaske, r3 = X"0000 000F" = 16 = 0000 ... 0000 1111
        add.i r5,r0,5                            ;- Tetradenvergleichswert, r5 = 5
        add.i r7,r0,3                            ;- Tetradenkorrekturwert r7 = 3;

;; BCD-Korrektur:
;; -------------        
;- hier wird über die Laufinvariante iteriert und jede stelle betrachtet 

bcd     sub.i r9, r9,1               ;- i -= 1
        add.i r10,r0,0-4             ;- Tetradenindex, Vorzeichenerweiterung, r10 = 1111 1111 1111 1100
        srl r13,r1,r9                ;- hole entsprechendes Bit aus Dualzahl
        and.i r14,r13,1              ;
        sll.i r2,r2,1                ;- bcd << 1
        or r2,r14,r2                 ;- ((dual>>i) & 0x1) | bcd , setze Stelle von bcd auf das entsprechendes Bit von dual
        beqz r9, w_bcd               ;- schreibe umgewandelte Fibonaccizahl

;- zwei Durchläufe, erster: if((bcd &  0xf)    >    0x4), zweiter: if((bcd &  0xf0)   >   0x40)
te_ko   add.i r10,r10,4              ;- Tetradenindex + 4, um die nächste BCD stelle zu bearbeiten
        sub.i r12,r10,(digits-1) * 4 ;- digits = 2, also ziehe vom aktuellen tetradenindex 4 ab und speichere in r12
        sll r4,r3,r10                ;- logischer Linksshift initialen Tetradenmaske um um den Tetradenindex, speicher in akuteller Tetradenmaske
        sll r6,r5,r10                ;- den initialen Tetradenvergleichswert um die stellen des Tetradenindex logisch links verschieben und im aktuellen speichern,
        sll r8,r7,r10                ;- setzt den richtigen Wert in r8 für plus_3 ( 0x3 und 0x30 )
        and r14,r2,r4                ;- 
        slt r11,r14,r6               ;- setze Sprungbedingung r11 auf 0 falls das aktuelle Dualzahlbit kleiner als der aktuelle vergleichwert ist, sprich wenn aktuelle BCD-Stelle > 4 wird gesprungen
        beqz r11,plus_3              ;- falls Aktuelle Tetrade sich als Pseudotetrade herrausstellt, zur Additionsroutine springen
        beqz r12,bcd                 ;- Wenn jede Stelle betrachtet wurde, gehe zum nächsten Schleifendurchlauf
        j te_ko                      ;- mache mit nächster Stelle weiter

;- addiere 0x3 oder 0x30 auf bcd
plus_3  add r2,r2,r8                 ;- bcd += r8
        beqz r12,bcd                 ;- Wenn jede Stelle betrachtet wurde, gehe zum nächsten Schleifendurchlauf
        j te_ko                      ;- mache mit nächster Stelle weiter

w_bcd   sw.i zero(r30),r2            ;- schreibe in RAM
        add.i r30, r30, 4            ;- auf nächsten freien Speicher zeigen
        sub.i r17, r17, 1            ;- counter -= 1
        beqz r17, ende               ;- wenn alle ausgeben wurden brich ab
        j ini_bcd                    ;- nächste Zahl

ende    j ende ;- Endschleife um Prozessor nicht abstürzen zu lassen
end

