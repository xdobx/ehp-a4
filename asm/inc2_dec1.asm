;----------------------------------------------------------------;
; Datei         : inc2dec1.asm                                   ;
; Datum         : Mar. 2010                                      ;
; Beschreibung  : Ein kleines Testprogram, mit dem die Folge     ;
;                 0 2 1 3 2 4 3 5 ...                            ;
;                 auf dem Display ausgegeben wird.               ;
;----------------------------------------------------------------;

        org   X"00000000"  ; legt die Programmstartadresse fest 
        start init         ; markiert den ersten Befehl
         
; Immediate-Werte fuer Speicheroperationen:
        zero  equ  X"0000" ; Offset 0 
        disp  equ  X"0FFC" ; Offset 0x0FFC
        ram   equ  X"0FF8" ; Offset 0x0FF8
;
; Es gibt nur eine Adressierungsart: Basisregister + 16 Bit Offset 
; mit Vorzeichen. Demzufolge kann auf Adressen, deren Wert 15 Bit
; uebersteigt, nur zugegriffen werden, wenn zuvor ein Basisregister 
; entsprechend initialisiert wurde. Deshalb sind die letzten beiden 
; ROM-Speicherzellen mit zwei Konstanten initialisiert. In der 
; Speicherzelle mit der Adresse 0x0000_0FFC steht die Adresse fuer 
; das Display Latch: 
;  M[0x0000_0FFC] = 0x2000_0000 
; und in Speicherzelle mit der Adresse 0x0000_0FF8 die Basisadresse 
; des RAM:
; M[0x0000_0FF8] = 0x1000_0000 

init:	
	lw.i    r31, disp(r0) 	; r31      <- M[0x0000_0FFC]      
	sw.i	zero(r31), r31	; Display  <- 0x2000_0000 
	slt 	r2, r0, r31 	; r2       <- 1 wegen r0 < r31
	sub	r1, r0, r2 	; r1       <- r0-r2 
	sub	r3, r1, r2 	; r3       <- r1-r2 
	slt 	r4, r0, r0 	; r4       <- 0
label_1	sub	r4, r4, r3 	; r4       <- r4-r3 
	sw.i	zero(r31), r4	; Display  <- r4 
	sub	r4, r4, r2 	; r4       <- r4-r2 
	sw.i	zero(r31), r4	; Display  <- r4 
		j label_1       ; springt zu label_1
	end






